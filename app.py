from subprocess import Popen, PIPE, STDOUT
import os
import subprocess
import sys
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5.QtWidgets import QHeaderView, QMainWindow, QApplication, QWidget, QAction, QTableWidget, QTableWidgetItem, QVBoxLayout
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QStringListModel, pyqtSlot
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
from multiprocessing import Process, Queue
import sh
import re
import pyperclip
import psutil
import functools


class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        print(f'event type: {event.event_type}  path : {event.src_path}')


class App(QWidget):

    def center(self):
        frameGm = self.frameGeometry()
        screen = app.desktop().screenNumber(
            app.desktop().cursor().pos())
        centerPoint = app.desktop().screenGeometry(screen).center()
        frameGm.moveCenter(centerPoint)
        self.move(frameGm.topLeft())

    def __init__(self):
        super().__init__()
        self.title = 'Linux History Commands'

        self.width = 1400
        self.height = 750
        self.setWindowModality(QtCore.Qt.ApplicationModal)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)
        self.initUI()

    def initUI(self):
        self.setWindowTitle(self.title)

        self.resize(self.width, self.height)
        self.center()

        self.createTable()

        # Add box layout, add table to box layout and add box layout to widget
        self.layout = QVBoxLayout()
        self.layout.addWidget(self.tableWidget)
        self.setLayout(self.layout)

        # Show widget
        self.show()

    def createTable(self):
       # Create table
        self.tableWidget = QTableWidget()

        self.tableWidget.setColumnCount(2)
        # self.tableWidget.setItem(0, 0, QTableWidgetItem("Time"))
        # self.tableWidget.setItem(0, 1, QTableWidgetItem("Command"))
        self.tableWidget.move(0, 0)
        header = self.tableWidget.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        self.tableWidget.setHorizontalHeaderItem(0, QTableWidgetItem("User"))
        self.tableWidget.setHorizontalHeaderItem(1, QTableWidgetItem("Root"))

        self.fillTimer = QtCore.QTimer()
        self.fillTimer.setInterval(1000)
        timerCallback = functools.partial(
            self.getTableData, widget=self.tableWidget)
        self.fillTimer.timeout.connect(timerCallback)
        self.fillTimer.start()

        # table selection change
        self.tableWidget.clicked.connect(self.on_click)

    def getTableData(self, widget: QTableWidget):    
        result = q.get()
        widget.setRowCount(0)
        i = 0
        for data in result:
            widget.insertRow(i)
            widget.setItem(i, 0, QTableWidgetItem(data))
            i = i+1

    @pyqtSlot()
    def on_click(self):
        for currentQTableWidgetItem in self.tableWidget.selectedItems():
            pyperclip.copy(currentQTableWidgetItem.text())



def fileHandler(q):
    while True:
        proc = subprocess.Popen("less ~/.zsh_history", shell=True,
                                stdout=subprocess.PIPE)
        output = proc.stdout.read().decode("utf-8")
        l1 = output.split("\n")
        l1.reverse()
        seen = set()
        notDup = []
        for f in l1:
            f = re.sub(r':.\d{10}:\d;', '', f)
            if(f not in seen):
                seen.add(f)
                notDup.append(f)
        q.put(notDup)
        time.sleep(1)


if __name__ == '__main__':
    result = []
    q = Queue()
    fileHandlerProcess = Process(
        target=fileHandler, args=(q,))
    fileHandlerProcess.daemon = True
    fileHandlerProcess.start()
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())
